// get the ninja-keys element
const ninja = document.querySelector('ninja-keys');

// add the home and posts menu items
ninja.data = [{
    id: "nav-about",
    title: "About",
    section: "Navigation",
    handler: () => {
      window.location.href = "/";
    },
  },{id: "nav-consultancy",
          title: "Consultancy",
          description: "Consultancy services provided by ASERG",
          section: "Navigation",
          handler: () => {
            window.location.href = "/consultancy/";
          },
        },{id: "nav-news",
          title: "News",
          description: "The ASERG news",
          section: "Navigation",
          handler: () => {
            window.location.href = "/news/";
          },
        },{id: "nav-people",
          title: "People",
          description: "The people that make ASERG.",
          section: "Navigation",
          handler: () => {
            window.location.href = "/people/";
          },
        },{id: "nav-publications",
          title: "Publications",
          description: "Here you can find the publications (Journals, Books and Conferences) by members of ASERG.",
          section: "Navigation",
          handler: () => {
            window.location.href = "/publications/";
          },
        },{id: "nav-projects",
          title: "Projects",
          description: "The different projects ASERG members are involved with.",
          section: "Navigation",
          handler: () => {
            window.location.href = "/projects/";
          },
        },{id: "post-a-post-with-image-galleries",
      
        title: "a post with image galleries",
      
      description: "this is what included image galleries could look like",
      section: "Posts",
      handler: () => {
        
          window.location.href = "/sample-posts/2024/12/04/photo-gallery.html";
        
      },
    },{id: "news-we-are-alive-the-web-page-of-the-aserg-group-is-now-live",
          title: 'We are alive. The Web page of the ASERG group is now live....',
          description: "",
          section: "News",},{id: "news-aserg-research-seminar-investigating-fuzzy-methods-for-multilingual-speaker-identification",
          title: 'ASERG Research Seminar - Investigating fuzzy methods for multilingual speaker identification',
          description: "",
          section: "News",handler: () => {
              window.location.href = "/news/ASERG-Research-seminar/";
            },},{id: "news-aserg-at-the-shu-transforming-learning-conference",
          title: 'ASERG at the SHU Transforming Learning Conference',
          description: "",
          section: "News",handler: () => {
              window.location.href = "/news/SHU-TL-Conference/";
            },},{id: "news-aserg-research-seminar-improving-the-performance-of-feature-selection",
          title: 'ASERG Research Seminar - Improving the Performance of Feature Selection',
          description: "",
          section: "News",handler: () => {
              window.location.href = "/news/ASERG-research-seminar/";
            },},{id: "news-aserg-helps-lead-innovation",
          title: 'ASERG helps lead Innovation',
          description: "",
          section: "News",handler: () => {
              window.location.href = "/news/ASERG-DIfG/";
            },},{id: "news-our-paper-an-architecture-for-data-integrity-in-untrustworthy-social-networks-has-been-accepted-for-presentation-at-the-37th-acm-sigapp-symposium-on-applied-computing-the-paper-presents-an-architecture-for-independent-verification-of-content-integrity-in-social-networks-in-response-to-a-scenario-of-untrustworthy-provider-and-is-the-result-of-the-work-developed-by-angus-young",
          title: 'Our paper An architecture for data integrity in untrustworthy social networks has been...',
          description: "",
          section: "News",},{id: "news-two-phd-studentiship-at-aserg",
          title: 'Two PhD studentiship at ASERG',
          description: "",
          section: "News",handler: () => {
              window.location.href = "/news/2-phd-positions/";
            },},{id: "news-dr-márjory-da-costa-abreu-has-the-paper-exploring-bias-analysis-on-judicial-data-using-machine-learning-techniques-accepted-at-the-12th-international-conference-on-pattern-recognition-systems-the-paper-presents-an-analysis-of-using-classical-machine-learning-models-to-identify-bias-in-judicial-sentences-is-the-result-of-the-work-developed-by-bruno-silva-mphil-in-systems-and-computing-ufrn-brazil",
          title: 'Dr. Márjory da Costa Abreu has the paper Exploring Bias Analysis on Judicial...',
          description: "",
          section: "News",},{id: "news-dr-márjory-da-costa-abreu-has-the-paper-exploring-content-based-and-meta-data-analysis-for-detecting-fake-news-infodemic-a-case-study-on-covid-19-accepted-at-the-12th-international-conference-on-pattern-recognition-systems-the-paper-presents-an-analysis-of-using-a-combination-of-classical-machine-learning-models-and-deep-learning-techniques-as-well-as-a-selection-of-descriptive-text-features-to-identify-fake-news-related-to-covid-19-on-social-media-this-research-is-the-result-of-the-work-developed-by-ashish-garg-msc-big-data-analytics-shu",
          title: 'Dr. Márjory da Costa Abreu has the paper Exploring Content-Based and Meta-Data Analysis...',
          description: "",
          section: "News",},{id: "news-dr-carlos-da-silva-has-the-paper-a-box-analogy-technique-boat-for-agile-based-modelling-of-business-processes-accepted-at-the-industrial-innovation-track-of-the-ieee-international-requirements-engineering-2022-conference-the-paper-presents-a-technique-for-extracting-business-processes-during-interviews-as-part-of-the-requirements-elicitation-phase-and-describes-how-the-technique-has-been-developed-through-its-application-with-three-different-clients-the-work-is-a-collaboration-with-leisia-medeiros-yan-justino-and-eduardo-luiz-gomes",
          title: 'Dr. Carlos da Silva has the paper A Box Analogy Technique (BoAT) for...',
          description: "",
          section: "News",},{id: "news-aserg-at-the-shu-learning-teaching-and-assessment-2022-conference",
          title: 'ASERG at the SHU Learning, Teaching and Assessment 2022 Conference',
          description: "",
          section: "News",handler: () => {
              window.location.href = "/news/SHU-Dev-Process-LTA-Conference/";
            },},{id: "news-the-article-bpm2ddd-a-systematic-process-for-identifying-domains-from-business-processes-models-has-been-published-on-the-mdpi-journal-software-the-article-presents-a-technique-to-capture-bounded-contexts-and-their-relationships-into-ddd-context-maps-based-on-the-extraction-of-information-from-business-process-models-the-work-is-a-collaboration-between-carlos-eduardo-da-silva-soumya-basu-and-eduardo-luiz-gomes",
          title: 'The article BPM2DDD: A Systematic Process for Identifying Domains from Business Processes Models...',
          description: "",
          section: "News",},{id: "news-several-gta-phd-positions-offered-by-members-of-aserg",
          title: 'Several GTA PhD positions offered by members of ASERG',
          description: "Projects involving partnerships with environmental, sports psychology and public health.",
          section: "News",handler: () => {
              window.location.href = "/news/PhD-Positions-for-2023/";
            },},{id: "news-checkout-here-how-aserg-member-dr-soumya-basu-helped-with-guildhawk-s-digital-transformation-while-pivoting-from-a-language-service-business-to-a-technology-led-global-operations",
          title: 'Checkout here how ASERG member Dr. Soumya Basu helped with Guildhawk’s digital transformation...',
          description: "",
          section: "News",},{id: "news-the-book-chapter-an-iot-enabled-innovative-smart-parking-recommender-approach-has-been-published-on-the-taylor-amp-amp-francis-book-recommender-systems-a-multi-disciplinary-approach-the-chapter-presents-recopark-an-iot-based-smart-parking-recommender-solution-which-enables-cars-to-a-parking-space-automatically-across-cities-and-to-reserve-it-on-the-move-the-work-is-a-collaboration-between-soumya-basu-and-ajanta-das",
          title: 'The book chapter An IoT-Enabled Innovative Smart Parking Recommender Approach has been published...',
          description: "",
          section: "News",},{id: "news-two-funded-phd-positions-offered-by-members-of-aserg",
          title: 'Two funded PhD positions offered by members of ASERG',
          description: "",
          section: "News",handler: () => {
              window.location.href = "/news/PhD-Positions-for-2024/";
            },},{id: "news-aserg-awarded-a-new-2-year-ktp-project-with-cafeology-ltd",
          title: 'ASERG awarded a new 2-year KTP project with Cafeology Ltd',
          description: "",
          section: "News",handler: () => {
              window.location.href = "/news/KTP-Cafeology-news/";
            },},{id: "people-abdel-karim-al-tamimi",
          title: 'Abdel Karim Al Tamimi',
          description: "",
          section: "People",handler: () => {
              window.location.href = "/people/abdel-karim-al-tamimi/";
            },},{id: "people-angus-young",
          title: 'Angus Young',
          description: "",
          section: "People",handler: () => {
              window.location.href = "/people/angus-young/";
            },},{id: "people-brian-davis",
          title: 'Brian Davis',
          description: "",
          section: "People",handler: () => {
              window.location.href = "/people/brian-davis/";
            },},{id: "people-carlos-da-silva",
          title: 'Carlos Da Silva',
          description: "",
          section: "People",handler: () => {
              window.location.href = "/people/carlos-da-silva/";
            },},{id: "people-eduardo-luiz",
          title: 'Eduardo Luiz',
          description: "",
          section: "People",handler: () => {
              window.location.href = "/people/eduardo-luiz/";
            },},{id: "people-faiza-samreen",
          title: 'Faiza Samreen',
          description: "",
          section: "People",handler: () => {
              window.location.href = "/people/faiza-samreen/";
            },},{id: "people-hakan-kiziloz",
          title: 'Hakan Kiziloz',
          description: "",
          section: "People",handler: () => {
              window.location.href = "/people/hakan-kiziloz/";
            },},{id: "people-jack-carey",
          title: 'Jack Carey',
          description: "",
          section: "People",handler: () => {
              window.location.href = "/people/jack-carey/";
            },},{id: "people-james-harmson",
          title: 'James Harmson',
          description: "",
          section: "People",handler: () => {
              window.location.href = "/people/james-harmson/";
            },},{id: "people-kang-wang",
          title: 'Kang Wang',
          description: "",
          section: "People",handler: () => {
              window.location.href = "/people/kang-wang/";
            },},{id: "people-karol-kierzkowski",
          title: 'Karol Kierzkowski',
          description: "",
          section: "People",handler: () => {
              window.location.href = "/people/karol-kierzkowski/";
            },},{id: "people-marjory",
          title: 'Marjory',
          description: "",
          section: "People",handler: () => {
              window.location.href = "/people/marjory/";
            },},{id: "people-natasha-rajpurohit",
          title: 'Natasha Rajpurohit',
          description: "",
          section: "People",handler: () => {
              window.location.href = "/people/natasha-rajpurohit/";
            },},{id: "people-segun-orimoloye",
          title: 'Segun Orimoloye',
          description: "",
          section: "People",handler: () => {
              window.location.href = "/people/segun-orimoloye/";
            },},{id: "people-soumya-basu",
          title: 'Soumya Basu',
          description: "",
          section: "People",handler: () => {
              window.location.href = "/people/soumya-basu/";
            },},{id: "people-yan-justino",
          title: 'Yan Justino',
          description: "",
          section: "People",handler: () => {
              window.location.href = "/people/yan-justino/";
            },},{id: "projects-edudrive-rnp",
          title: 'edudrive@RNP',
          description: "A Cloud Computing storage service for academics and researchers of Brazilian institutions.",
          section: "Projects",handler: () => {
              window.location.href = "/projects/2015-12_edudrive/";
            },},{id: "projects-gt-ampto-multi-factor-authentication-to-shibboleth-identity-providers",
          title: 'GT-AMPTo - Multi-Factor Authentication to Shibboleth Identity Providers',
          description: "This project aims to develop a solution that allows the Brazilian Academic Federation (CAFe) to operate with multiple authentication factors on Shibboleth Identity Providers.",
          section: "Projects",handler: () => {
              window.location.href = "/projects/2017-05_gt-ampto/";
            },},{id: "projects-service-oriented-process-for-agile-development",
          title: 'Service Oriented Process for Agile Development',
          description: "A Software Development process for developing microservices following the Service-orientation principles.",
          section: "Projects",handler: () => {
              window.location.href = "/projects/2018-08_SPReaD/";
            },},{id: "projects-shu-dev-software-development-process-for-students",
          title: 'SHU-Dev: Software Development Process for Students',
          description: "A Software Development process that can be followed by students at all levels as they work on both individual and group assessments.",
          section: "Projects",handler: () => {
              window.location.href = "/projects/2020-10_SHUDevProcess/";
            },},{id: "projects-landscape-laboratory-data-infrastructure",
          title: 'Landscape Laboratory Data Infrastructure',
          description: "A smart data management infrastructure to support long-term research in the context of the Landscape Laboratory.",
          section: "Projects",handler: () => {
              window.location.href = "/projects/2022-11-SHU-LandscapeLab/";
            },},{
        id: 'social-email',
        title: 'email',
        section: 'Socials',
        handler: () => {
          window.open("mailto:%79%6F%75@%65%78%61%6D%70%6C%65.%63%6F%6D", "_blank");
        },
      },{
        id: 'social-inspire',
        title: 'Inspire HEP',
        section: 'Socials',
        handler: () => {
          window.open("https://inspirehep.net/authors/1010907", "_blank");
        },
      },{
        id: 'social-rss',
        title: 'RSS Feed',
        section: 'Socials',
        handler: () => {
          window.open("/feed.xml", "_blank");
        },
      },{
        id: 'social-scholar',
        title: 'Google Scholar',
        section: 'Socials',
        handler: () => {
          window.open("https://scholar.google.com/citations?user=qc6CJjYAAAAJ", "_blank");
        },
      },{
        id: 'social-custom_social',
        title: 'Custom_social',
        section: 'Socials',
        handler: () => {
          window.open("https://www.alberteinstein.com/", "_blank");
        },
      },{
      id: 'light-theme',
      title: 'Change theme to light',
      description: 'Change the theme of the site to Light',
      section: 'Theme',
      handler: () => {
        setThemeSetting("light");
      },
    },
    {
      id: 'dark-theme',
      title: 'Change theme to dark',
      description: 'Change the theme of the site to Dark',
      section: 'Theme',
      handler: () => {
        setThemeSetting("dark");
      },
    },
    {
      id: 'system-theme',
      title: 'Use system default theme',
      description: 'Change the theme of the site to System Default',
      section: 'Theme',
      handler: () => {
        setThemeSetting("system");
      },
    },];
